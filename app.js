const container = document.getElementById("container");
let cells = document.getElementsByClassName("cell");
console.log(container);
function defaultGrid(){
    makeRows(8);
    makeColumns(8);
};
defaultGrid();
function makeRows(rowNum){
    for(i=0; i < rowNum; i++){
        let row = document.createElement("cell");
        row.className="gridRow";
        container.appendChild(row); 
    };
};

function makeColumns(cellNum){
    let rows = document.getElementsByClassName("gridRow");
    for(i=0;i<rows.length;i++){
        for(j=0;j<cellNum;j++){
            let newCell = document.createElement("cell");
            newCell.className="cell";
            rows[j].appendChild(newCell);   
        };
    };
}

function printMousePos(event) {
    document.body.textContent =
      "clientX: " + event.clientX +
      " - clientY: " + event.clientY;
  }
  document.addEventListener("click", printMousePos);